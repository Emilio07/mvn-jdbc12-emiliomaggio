package net.tinvention.training.jse.application.ui;

import net.tinvention.training.jse.conf.LayerConfigurer;

public class ApplicationStarter {
	public static void main(String[] args) {
		// Alternate ways
		// Using layerConfgurer , for a IoC approach
		LayerConfigurer.getApplicaitonConfigured().start();

	}
}

