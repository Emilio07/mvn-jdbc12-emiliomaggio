	package net.tinvention.training.jse.persistence.impl;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

import net.tinvention.training.jse.dto.UtenteDto;
import net.tinvention.training.jse.persistence.GestioneTimbratureDao;

public class GestioneTimbratureDaoImpl implements GestioneTimbratureDao {
	private final String JDBCURL;
	private final String USERNAME;
	private final String PASSWORD;
	private Logger LOG = LogManager.getLogger(GestioneTimbratureDaoImpl.class);

	public GestioneTimbratureDaoImpl(String fileProp) {
		Properties p = new Properties();
		try {
			p.load(new FileInputStream(fileProp));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		this.JDBCURL = p.getProperty("configuration.jdbcurl");
		;
		this.USERNAME = p.getProperty("configuration.username");
		;
		this.PASSWORD = p.getProperty("configuration.password");
		;

	}

	public void aggiungiRigaTabellaDatabase(UtenteDto u) throws SQLException {
		Connection con = null;
		try {
			con = DriverManager.getConnection(JDBCURL, USERNAME, PASSWORD);
			PreparedStatement stmt = con
					.prepareStatement("insert into utenti (matricola,nome,cognome) VALUES (?,?,?);");
			stmt.setInt(1, u.getMatricola());
			stmt.setString(2, u.getNome());
			stmt.setString(3, u.getCognome());
			stmt.executeUpdate();
		} finally {
			if (con != null) {
				con.close();
			}
		}

	}

	public void eliminaRigaTabellaDatabase(int numMatricola) throws SQLException {
		try (Connection con = DriverManager.getConnection(JDBCURL, USERNAME, PASSWORD);
				PreparedStatement stmt = con.prepareStatement("delete from utenti where matricola = ?;");) {
			LOG.debug("metodo eliminaRigaTabellaDataBase richiamato a livello DAO");
			stmt.setInt(1, numMatricola);
			stmt.executeUpdate();
		}

	}

	public UtenteDto cercaUtenteTabellaDatabase(int numMatricola) throws SQLException {
		try (Connection con = DriverManager.getConnection(JDBCURL, USERNAME, PASSWORD);
				PreparedStatement stmt = con.prepareStatement("select* from utenti where matricola = ?;");) {
			stmt.setInt(1, numMatricola);
			UtenteDto u = null;
			try (ResultSet res = stmt.executeQuery()) {
				if (res.next()) {
					u = new UtenteDto(res.getInt("matricola"), res.getString("nome"), res.getString("cognome"),
							res.getInt("timbrature"));
				}
			}
			return u;
		}
	}

	public void aggiornaTimbratureTabellaDatabase(int numMatricola) throws SQLException {
		Connection con = DriverManager.getConnection(JDBCURL, USERNAME, PASSWORD);
		if (con == null) {
			throw new SQLException("non si  stabilita nessuna connessione");
		}
		PreparedStatement stmt = con.prepareStatement("update utenti set Timbrature= Timbrature+1 WHERE matricola=?;");
		stmt.setInt(1, numMatricola);
		stmt.executeUpdate();
		con.close();
	}

	public List<UtenteDto> restituisciListaUtentiTabellaDatabase() throws SQLException {
		try (Connection con = DriverManager.getConnection(JDBCURL, USERNAME, PASSWORD);
				PreparedStatement stmt = con.prepareStatement("select * from utenti");) {
			List<UtenteDto> lista = new ArrayList<UtenteDto>();
			try (ResultSet res = stmt.executeQuery()) {
				while (res.next()) {
					lista.add(new UtenteDto(res.getInt("matricola"), res.getString("nome"), res.getString("cognome"),
							res.getInt("timbrature")));
				}
			}
			return lista;
		}
	}

	public String getJDBCURL() {
		return JDBCURL;
	}

	public String getUSERNAME() {
		return USERNAME;
	}

	public String getPASSWORD() {
		return PASSWORD;
	}
}
