package net.tinvention.training.jse.bl;

import java.sql.SQLException;
import java.util.List;

import net.tinvention.training.jse.dto.UtenteDto;

public interface GestioneTimbratureLogic {

	public void aggiungiRigaTabellaUtenti(UtenteDto u) throws SQLException;

	public void eliminaRigaTabellaUtenti(int numMatricola) throws SQLException;

	public void aggiornaTimbratureTabellaUtenti(int numMatricola) throws SQLException;

	public UtenteDto ottieniUtenteTabellaUtenti(int numMatricola) throws SQLException;

	public List<UtenteDto> restituisciListaUtentiTabellaUtenti() throws SQLException;

}

